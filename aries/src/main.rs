use std::io::Read;
use std::{
    convert::AsRef,
    sync::{Arc, Mutex},
};
use std::{env, fs::File};

use crate::erro::Result;

use bytes::Bytes;
use gemini_proto::{Request, Response, ResponseMeta, GEMINI_MAX_REQ_LEN};
use getopts::Options;
use mesg::{FsMessage, FsRequest};
use rand;
use registration::Server;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpListener;
use tokio_native_tls::native_tls::{self, Identity};
use tokio_native_tls::TlsAcceptor;
use tracing::{debug, error, Level};
use tracing_subscriber::FmtSubscriber;

mod erro;
mod fs;
mod mesg;
mod registration;

fn main() {
    let args: Vec<String> = env::args().collect();
    let argv0 = &args[0];

    let mut opts = Options::new();
    opts.optflag("d", "debug", "set log level to debug");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => panic!(f.to_string()),
    };

    let level = if matches.opt_present("d") {
        Level::TRACE
    } else {
        Level::WARN
    };

    let subscriber = FmtSubscriber::builder().with_max_level(level).finish();
    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");

    // root server
    if matches.free.len() == 0 {
        let rt = tokio::runtime::Runtime::new().unwrap();
        let future = inner_main(argv0);
        if let Err(err) = rt.block_on(future) {
            error!(error = %err, "Got fatal error, aborting");
            std::process::exit(1);
        }
    } else {
        match matches.free[0].as_str() {
            "fs" => fs::entrypoint(&matches.free[1..]),
            _ => panic!("invalid arg"),
        };
    }
}

async fn inner_main(argv0: &str) -> Result<()> {
    let servers = [Server {
        argv0: &argv0,
        name: registration::ServerType::Fs,
        args: &[],
    }];

    debug!("Starting all servers");
    let chans_mtx = Arc::new(Mutex::new(registration::registration(&servers).await?));
    debug!("Started servers");

    // read privkey stuff
    debug!("Reading TLS files");
    let mut fil = File::open("identity.pfx")?;
    let mut fil_contents = vec![];
    fil.read_to_end(&mut fil_contents)?;

    debug!("Listening on port 1965");
    // Set up TLS sockets
    let listener = TcpListener::bind("127.0.0.1:1965").await?;
    let identity = Identity::from_pkcs12(&fil_contents, "")?;
    let acceptor = Arc::new(TlsAcceptor::from(native_tls::TlsAcceptor::new(identity)?));
    debug!("Ready to accept requests");

    loop {
        let (socket, addr) = listener.accept().await?;
        let acceptor = acceptor.clone();
        let chans_mtx = chans_mtx.clone();
        debug!(?addr, "got connection");

        tokio::spawn(async move {
            let mut tls_stream = acceptor.accept(socket).await.expect("accept error");
            let mut buf: [u8; GEMINI_MAX_REQ_LEN] = [0; GEMINI_MAX_REQ_LEN];
            let reqid = rand::random::<u64>();
            debug!(?reqid, "accepted connection");

            let n = match tls_stream.read(&mut buf).await {
                Ok(n) if n == 0 => return,
                Ok(n) => n,
                Err(e) => {
                    eprintln!("failed to read from socket; err = {:?}", e);
                    return;
                }
            };

            // TODO: CRLF validation
            let req = Request::new(buf).expect("can't build request");
            debug!(?req, ?n, "got request");

            let path = req.decoded_path().expect("can't decode");

            // This current impl sucks
            //
            // New plan going forward:
            // 1. Create a pool for requests (hash of reqid to channels). Pool entry should live as long as
            // a request, and get dropped after its done.
            // 1.1. Channels will be created for each connection. Hopefully threads do not have to be.
            // 2. Have a task for watching fs readers, and send results to channel on completion
            // 3. Have a task for sending responses, where task selects on all channels?
            let mut response: Option<Response> = None;
            {
                let chans = chans_mtx.lock().expect("couldn't get lock");
                // TODO: replace with hash lookup
                for chan in &chans.fs_channels {
                    debug!(chanpath = ?chan.path, ?path);
                    if path.contains(&chan.path) {
                        debug!("Sending fs command");
                        chan.inner
                            .tx
                            .send(FsMessage::Request(FsRequest {
                                reqid,
                                path: String::from(path),
                            }))
                            .expect("couldn't send");

                        loop {
                            debug!("Waiting to receive fs response");
                            let resp = chan.inner.rx.recv().expect("couldn't receive");
                            match resp {
                                FsMessage::Response(mesg) => {
                                    match mesg {
                                        mesg::FsResponse::Success { reqid: rx_reqid, contents: _, mime_type: _ } if rx_reqid != reqid => {
                                            debug!(reqid = rx_reqid, "Got response for different request");
                                            continue;
                                        }
                                        mesg::FsResponse::Failure { reqid: rx_reqid, error: _ } if rx_reqid != reqid => {
                                            debug!(reqid = rx_reqid, "Got response for different request");
                                            continue;
                                        }
                                        mesg::FsResponse::Success {reqid, contents, mime_type } => {
                                            debug!(%reqid, "Got response from fs server");
                                            response = Some(Response {
                                                    meta: ResponseMeta::Success(Bytes::from(mime_type)),
                                                    body: Some(Bytes::from(contents))
                                                });
                                        }
                                        mesg::FsResponse::Failure { reqid: _, error } => {
                                            debug!(%reqid, ?error, "Got error from fs server");
                                            response = Some(Response {
                                                meta: ResponseMeta::TempFailure(Some(Bytes::from(error))),
                                                body: None,
                                            });
                                        }
                                    }

                                    break;
                                }
                                FsMessage::Request(mesg) => {
                                    debug!(?mesg, "impossible");
                                    continue;
                                }
                            }
                        }

                        break;
                    }
                }
            }
            debug!(?response, "sending response");

            if let Some(res) = response {
                let b: Bytes = res.into();
                tls_stream.write(b.as_ref()).await.expect("write error");
                tls_stream.flush().await.expect("flush err");
                debug!("wrote response");
            }
        });
    }
}
