use std::path::Path;
use std::{
    collections::HashMap,
    convert::AsRef,
    path::PathBuf,
    sync::{Arc, RwLock},
};

use crate::{
    erro::{Error, Result},
    mesg::{FsMessage, FsResponse, RegistrationMesg},
};

use ipc_channel::ipc::{self, IpcSender};
use tokio::fs::File;
use tokio::io::AsyncReadExt;
use tracing::{debug, error};

pub(crate) fn entrypoint(args: &[String]) {
    if args.len() == 0 {
        panic!("Requires a UNIX socket as a parameter");
    }

    let socket = &args[0];

    unveil::unveil("/usr/bin/file", "x").expect("Could not unveil");
    unveil::unveil(".", "r").expect("Could not unveil");
    unveil::unveil(socket, "r").expect("Could not unveil");
    unveil::unveil("", "").expect("Could not unveil");

    pledge::pledge_promises![Exec Proc Rpath Sendfd Stdio Unix].expect("Could not pledge");

    debug!("Starting fs server");
    let rt = tokio::runtime::Runtime::new().unwrap();
    let future = inner_entrypoint(socket.clone());
    if let Err(e) = rt.block_on(future) {
        error!(error = %e, "fs server terminating");
        std::process::exit(1);
    }
}

// THIS IS SINGLE THREADED?
async fn inner_entrypoint(server_name: String) -> Result<()> {
    let registration_tx: IpcSender<RegistrationMesg<FsMessage>> =
        IpcSender::connect(server_name).unwrap();

    let (server_tx, command_rx) = ipc::channel()?;
    let (result_tx, server_rx) = ipc::channel()?;

    registration_tx.send(RegistrationMesg {
        tx: server_tx,
        rx: server_rx,
    })?;

    pledge::pledge_promises![Exec Proc Rpath Stdio].expect("Could not pledge");

    let mime_cache: Arc<RwLock<HashMap<PathBuf, String>>> =
        Arc::new(RwLock::new(HashMap::new()));

    debug!("Listening for requests");
    loop {
        let data = command_rx.recv()?;
        debug!(?data, "Received request");
        let mime_cache = mime_cache.clone();
        match data {
            FsMessage::Request(mesg) => {
                debug!(reqid = mesg.reqid, "Processing request");
                let resp = match lookup_path(mime_cache, mesg.path.as_ref()).await {
                    Ok((mime, contents)) => FsResponse::Success {
                        reqid: mesg.reqid,
                        contents,
                        mime_type: mime,
                    },
                    Err(err) => FsResponse::Failure {
                        reqid: mesg.reqid,
                        error: format!("{}", err),
                    },
                };

                debug!(
                    reqid = mesg.reqid,
                    ?mesg,
                    "Sending response back to main server"
                );
                result_tx.send(FsMessage::Response(resp))?;
            }
            FsMessage::Response(mesg) => {
                debug!(?mesg, "invalid message");
                continue;
            }
        };
    }
}

async fn lookup_path(mime_cache: Arc<RwLock<HashMap<PathBuf, String>>>, pathname: &Path) -> Result<(String, Vec<u8>)> {
    debug!(?pathname, "Reading file");
    let path = pathname.strip_prefix("/").unwrap();
    let mut file = File::open(path).await?;
    let mut buf = Vec::new();
    file.read_to_end(&mut buf).await?;
    debug!("Read file without error");

    Ok((discover_mime(mime_cache, path).await?, buf))
}

async fn discover_mime(mime_cache: Arc<RwLock<HashMap<PathBuf, String>>>, pathname: &Path) -> Result<String> {
    {
        if let Some(cache_entry) = mime_cache.read().unwrap().get(pathname) {
            return Ok(cache_entry.clone());
        }
    }

    let output = tokio::process::Command::new("/usr/bin/file")
        .arg("--mime-type")
        .arg(pathname)
        .kill_on_drop(true)
        .output()
        .await?;

    if !output.status.success() {
        Err(Error::new(format!(
            "/usr/bin/file exited with: {}",
            output.status
        )))
    } else {
        let output = String::from_utf8(output.stdout)?;
        if let Some(mime) = output.split(": ").last() {
            let mime = mime.trim();
            debug!(?mime, "got mime");
            {
                mime_cache.write().unwrap().insert(PathBuf::from(pathname), mime.to_string());
            }
            Ok(mime.to_string())
        } else {
            Err(Error::new(format!(
                "malformed output from /usr/bin/file: {}",
                output
            )))
        }
    }
}
