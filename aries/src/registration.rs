use std::time::Duration;

use crate::mesg::RegistrationMesg;
use crate::{erro::Result, mesg::FsMessage};
use ipc_channel::ipc::{IpcOneShotServer, IpcReceiver, IpcSender};
use tokio::{process::Command, time};
use tracing::debug;

#[derive(Debug, Copy, Clone)]
pub(crate) enum ServerType {
    Fs,
}

impl ServerType {
    fn as_str(&self) -> &'static str {
        match *self {
            ServerType::Fs => "fs",
        }
    }
}

#[derive(Debug)]
pub(crate) struct Server<'a> {
    pub argv0: &'a str,
    pub name: ServerType,
    pub args: &'a [String],
}

#[derive(Debug)]
pub(crate) struct Channel<T> {
    pub name: ServerType,
    pub tx: IpcSender<T>,
    pub rx: IpcReceiver<T>,
}

#[derive(Debug)]
pub(crate) struct FsChannel {
    pub inner: Channel<FsMessage>,
    pub path: String,
}

pub(crate) struct RegisteredChannels {
    pub fs_channels: Vec<FsChannel>,
    // receiver sets will go here
    // for example
    //proxy_channels: Channel<String>,
}

/// Create assortment of oneshotservers
pub(crate) async fn registration<'a>(servers: &'a [Server<'a>]) -> Result<RegisteredChannels> {
    let mut ret = RegisteredChannels {
        fs_channels: Vec::new(),
    };

    for server in servers {
        debug!(server_type = server.name.as_str(), "Creating server");
        let (reg_server, server_name): (IpcOneShotServer<RegistrationMesg<_>>, String) =
            IpcOneShotServer::new().expect("could not start server");

        start_server(server, &server_name).await?;

        let (_, data) = reg_server.accept().expect("accept");

        match server.name {
            ServerType::Fs => {
                ret.fs_channels.push(FsChannel {
                    inner: Channel {
                        name: server.name,
                        tx: data.tx,
                        rx: data.rx,
                    },
                    path: String::from("/"),
                });
            }
        };
    }

    Ok(ret)
}

/// Create new server and register it
async fn start_server<'a>(server: &'a Server<'a>, server_name: &'a str) -> Result<()> {
    debug!(
        "Running {:?} -d {:?} {:?} {:?}",
        server.argv0, server.name, server_name, server.args
    );
    let mut fscmd = Command::new(server.argv0)
        .arg("-d")
        .arg(server.name.as_str())
        .arg(server_name)
        .args(server.args)
        .spawn()?;

    // wait
    time::sleep(Duration::from_secs(1)).await;
    fscmd.try_wait()?;

    debug!("Child started");

    Ok(())
}
