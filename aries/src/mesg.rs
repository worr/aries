use ipc_channel::ipc::{IpcSender, IpcReceiver};
use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct RegistrationMesg<T> {
    pub tx: IpcSender<T>,
    pub rx: IpcReceiver<T>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct FsRequest {
    pub reqid: u64,
    pub path: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) enum FsResponse {
    Success {
        reqid: u64,
        contents: Vec<u8>,
        // TODO: revisit type here
        mime_type: String,
    },
    Failure {
        reqid: u64,
        // TODO: revisit type here
        error: String,
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) enum FsMessage {
    Request(FsRequest),
    Response(FsResponse),
}
