use std::error;
use std::fmt;
use std::io;
use std::result;
use std::{convert, string};

use either::{Either, Left, Right};
use tokio_native_tls::native_tls;

pub(crate) type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
pub(crate) enum Error {
    IoError(io::Error),
    NativeTlsError(native_tls::Error),
    FromUtf8Error(string::FromUtf8Error),
    IpcError(Either<ipc_channel::Error, IpcErrorWrapper>),
    AriesError(String),
}

impl Error {
    pub fn new(desc: String) -> Self {
        Error::AriesError(desc)
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            Error::NativeTlsError(ref err) => Some(err),
            Error::IoError(ref err) => Some(err),
            Error::FromUtf8Error(ref err) => Some(err),
            Error::IpcError(ref err) => Some(err),
            Error::AriesError(_) => None,
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Error::IoError(ref err) => write!(f, "IO Error: {}", err),
            Error::NativeTlsError(ref err) => write!(f, "TLS Error: {}", err),
            Error::FromUtf8Error(ref err) => write!(f, "UTF-8 decoding Error: {}", err),
            Error::IpcError(ref err) => write!(f, "IPC Error: {}", err),
            Error::AriesError(ref desc) => write!(f, "{}", desc),
        }
    }
}

impl convert::From<native_tls::Error> for Error {
    fn from(err: native_tls::Error) -> Self {
        Error::NativeTlsError(err)
    }
}

impl convert::From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Error::IoError(err)
    }
}

impl convert::From<string::FromUtf8Error> for Error {
    fn from(err: string::FromUtf8Error) -> Self {
        Error::FromUtf8Error(err)
    }
}

impl convert::From<ipc_channel::Error> for Error {
    fn from(err: ipc_channel::Error) -> Self {
        Error::IpcError(Left(err))
    }
}

impl convert::From<ipc_channel::ipc::IpcError> for Error {
    fn from(err: ipc_channel::ipc::IpcError) -> Self {
        Error::IpcError(Right(IpcErrorWrapper(err)))
    }
}

#[derive(Debug)]
pub(crate) struct IpcErrorWrapper(ipc_channel::ipc::IpcError);

impl error::Error for IpcErrorWrapper {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self.0 {
            ipc_channel::ipc::IpcError::Bincode(ref err) => Some(err),
            ipc_channel::ipc::IpcError::Io(ref err) => Some(err),
            ipc_channel::ipc::IpcError::Disconnected => None
        }
    }
}

impl fmt::Display for IpcErrorWrapper {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.0 {
            ipc_channel::ipc::IpcError::Bincode(ref err) => write!(f, "{}", err),
            ipc_channel::ipc::IpcError::Io(ref err) => write!(f, "{}", err),
            ipc_channel::ipc::IpcError::Disconnected => write!(f, "Disconnected"),
        }
    }
}
