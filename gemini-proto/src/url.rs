use std::{borrow::Cow, convert::TryFrom};

use crate::erro::*;

use percent_encoding;
use url;

#[derive(Debug, Eq, PartialEq)]
pub struct Url {
    inner: url::Url,
}

impl Url {
    pub fn new(url: &str) -> Result<Self> {
        let inner = url::Url::parse(url)?;
        Url::try_from(inner)
    }

    pub fn host_str(&self) -> Option<&str> {
        self.inner.host_str()
    }

    pub fn port(&self) -> Option<u16> {
        self.inner.port()
    }

    pub fn path(&self) -> &str {
        self.inner.path()
    }

    pub fn decoded_path(&self) -> Result<Cow<str>> {
        Ok(percent_encoding::percent_decode(self.inner.path().as_bytes()).decode_utf8()?)
    }
}

impl TryFrom<url::Url> for Url {
    type Error = Error;

    fn try_from(val: url::Url) -> Result<Self> {
        if val.scheme() != "gemini" {
            return Err(Error::new(format!(
                "Invalid scheme: '{}', expected 'gemini://'",
                val.scheme()
            )));
        }

        Ok(Url { inner: val })
    }
}

impl TryFrom<&str> for Url {
    type Error = Error;

    fn try_from(val: &str) -> Result<Self> {
        Ok(Self::try_from(url::Url::parse(val)?)?)
    }
}

#[cfg(test)]
mod tests {
    use super::Url;
    use std::convert::TryFrom;
    use url;

    #[test]
    fn try_from_url() {
        let ok = Url::try_from(url::Url::parse("gemini://foo.bar").unwrap());
        assert!(ok.is_ok());
        assert!(ok.unwrap().inner.as_str() == "gemini://foo.bar")
    }

    #[test]
    fn try_from_url_bad_scheme() {
        let ok = Url::try_from(url::Url::parse("https://foo.bar").unwrap());
        assert!(ok.is_err());
        assert_eq!(
            format!("{}", ok.err().unwrap()),
            "Invalid scheme: 'https', expected 'gemini://'"
        );
    }

    #[test]
    fn try_from_str() {
        let ok = Url::try_from("gemini://foo.bar");
        assert!(ok.is_ok());
        assert!(ok.unwrap().inner.as_str() == "gemini://foo.bar")
    }

    #[test]
    fn try_from_str_bad_scheme() {
        let ok = Url::try_from("https://foo.bar");
        assert!(ok.is_err());
        assert_eq!(
            format!("{}", ok.err().unwrap()),
            "Invalid scheme: 'https', expected 'gemini://'"
        );
    }
}
