use std::{borrow::Cow, convert::AsRef};

use crate::url::Url;

use bytes::{BufMut, Bytes, BytesMut};
use std::str;

mod erro;
mod url;

use erro::Result;

/// Spec specifies URL is 1024 bytes + <CR><LF>
pub const GEMINI_MAX_REQ_LEN: usize = 1026;

#[derive(Debug, Eq, PartialEq)]
pub struct Request {
    url: Url,
}

impl Request {
    pub fn new<T: AsRef<[u8]>>(b: T) -> Result<Self> {
        // Converting to a string should be safe since
        // this is either UTF-8 or punycode.
        let s = str::from_utf8(b.as_ref())?;
        Ok(Request { url: Url::new(s)? })
    }

    pub fn url(&self) -> &Url {
        &self.url
    }

    pub fn path(&self) -> &str {
        self.url.path()
    }

    pub fn decoded_path(&self) -> Result<Cow<str>> {
        self.url.decoded_path()
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum ResponseMeta {
    // 1X
    Input(Bytes),
    SensitiveInput(Bytes),

    // 2X
    Success(Bytes),

    // 3X
    TempRedirect(Url),
    PermRedirect(Url),

    // 4X
    TempFailure(Option<Bytes>),
    ServerUnavailable(Option<Bytes>),
    CGIError(Option<Bytes>),
    ProxyError(Option<Bytes>),
    SlowDown(Option<Bytes>),

    // 5X
    PermFailure(Option<Bytes>),
    NotFound(Option<Bytes>),
    Gone(Option<Bytes>),
    ProxyRequestRefused(Option<Bytes>),
    BadRequest(Option<Bytes>),

    // 6X
    ClientCertificateRequired(Option<Bytes>),
    CertificateNotAuthorised(Option<Bytes>),
    CertificateNotValid(Option<Bytes>),
}

#[derive(Debug, Eq, PartialEq)]
pub struct Response {
    pub meta: ResponseMeta,
    pub body: Option<Bytes>,
}

impl From<Response> for Bytes {
    fn from(res: Response) -> Self {
        let mut ret = BytesMut::new();
        match res.meta {
            ResponseMeta::Input(prompt) => {
                ret.put(&b"10 "[..]);
                ret.put(prompt);
            }
            ResponseMeta::SensitiveInput(_) => {}
            ResponseMeta::Success(mime_type) => {
                ret.put(&b"20 "[..]);
                ret.put(mime_type);
                ret.put(&b"\r\n"[..]);
                if let Some(body) = res.body {
                    ret.put(body);
                }
            }
            ResponseMeta::TempRedirect(_) => {}
            ResponseMeta::PermRedirect(_) => {}
            ResponseMeta::TempFailure(_) => {}
            ResponseMeta::ServerUnavailable(_) => {}
            ResponseMeta::CGIError(_) => {}
            ResponseMeta::ProxyError(_) => {}
            ResponseMeta::SlowDown(_) => {}
            ResponseMeta::PermFailure(_) => {}
            ResponseMeta::NotFound(_) => {}
            ResponseMeta::Gone(_) => {}
            ResponseMeta::ProxyRequestRefused(_) => {}
            ResponseMeta::BadRequest(_) => {}
            ResponseMeta::ClientCertificateRequired(_) => {}
            ResponseMeta::CertificateNotAuthorised(_) => {}
            ResponseMeta::CertificateNotValid(_) => {}
        };

        ret.freeze()
    }
}
