use std::error;
use std::fmt;
use std::result;
use std::str;
use url;

#[derive(Debug)]
pub enum Error {
    UrlParseError(url::ParseError),
    Utf8Error(str::Utf8Error),
    CustomError(String),
}

impl Error {
    pub fn new(desc: String) -> Self {
        Error::CustomError(desc)
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            Error::UrlParseError(ref err) => Some(err),
            Error::Utf8Error(ref err) => Some(err),
            Error::CustomError(_) => None,
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::UrlParseError(ref err) => write!(f, "Error parsing Gemini URL: {}", err),
            Error::Utf8Error(ref err) => write!(f, "Error converting from utf8: {}", err),
            Error::CustomError(ref desc) => write!(f, "{}", desc),
        }
    }
}

impl From<url::ParseError> for Error {
    fn from(err: url::ParseError) -> Self {
        Error::UrlParseError(err)
    }
}

impl From<str::Utf8Error> for Error {
    fn from(err: str::Utf8Error) -> Self {
        Error::Utf8Error(err)
    }
}

pub type Result<T> = result::Result<T, Error>;
